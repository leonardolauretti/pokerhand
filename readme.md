Royal Straight Flush:
    The highest five denominations all in the same suit

Straight Flush:
    Five cards in denomination sequence, all in the same suit

Four of a Kind:
    Four cards of the same denomination

Full House:
    Three cards of one denomination and two of another denomination

Flush:
    Five cards in the same suit but not in denomination sequence

Straight:
    Five cards in denomination sequence

Three of a Kind:
    Three cards of the same denomination

Two Pair:
    Two cards of one denomination and two cards of another denomination

One Pair:
    Two cards of the same denomination

High Card:
    Hand with the card of the highest denomination. Tie breakers go to the next highest card etc.
