class Card:

    # Lowest to highest
    VALUE_ORDER = ['2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A']
    # Lowest to highest
    SUIT_ORDER = ['S', 'H', 'D', 'C']

    def __init__(self, value, suit):
        self.value = value
        self.suit = suit
        self.value_rank = self.VALUE_ORDER.index(value)
        self.suit_rank = self.SUIT_ORDER.index(suit)

    def __repr__(self):
        return '(' + self.value + ', ' + self.suit + ')'

    def get_rank(self):
        return [self.value_rank, self.suit_rank]
