from poker_hand import PokerHand
from result import Result

#print(PokerHand('TC TH 5C 5H KH').is_royal_flush())
#print(PokerHand('TC TH 5C 5H KH').is_same_suit())
#PokerHand('TC TH 5C 5H KH').is_same_suit()
#PokerHand('TC TH 5C 5H KH').sort_by_suit()
#print(PokerHand('TC TC 5C 5C KC').is_same_suit())
#print(PokerHand('AC KC QC JC TC').is_royal_flush())
#print(PokerHand('5C 6C 7C 8C 9C').is_straight())
#print(PokerHand('TC 9H 8C 7D 6C').is_straight())
#print(PokerHand('TC TH TH TC 6C').is_four_of_a_kind())
#print(PokerHand('QC QH QH 5C 5C').rank())
PokerHand("TC TH 5C 5H KH").compare_with(PokerHand("9C 9H 5C 5H AC"))
PokerHand("TS TD KC JC 7C").compare_with(PokerHand("JS JC AS KC TD"))