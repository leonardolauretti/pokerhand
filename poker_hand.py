from card import Card

class PokerHand:

    # Highest to lowest
    RANK_ORDER = [
        'royal_flush',
        'straight_flush',
        'four_of_a_kind',
        'full_house',
        'flush',
        'straight',
        'three_of_a_kind',
        'two_pair',
        'one_pair',
    ]

    def __init__(self, cards):
        self.cards = [Card(list(card_string)[0], list(card_string)[1]) for card_string in cards.split(' ')]
        print(self.cards)

    def sort(self, sort_by = 'suit', cards = None):
        if cards is None: cards = self.cards
        sorted_cards = list(self.cards)
        if sort_by == 'suit':
            sort_lambda = lambda x: (x.suit_rank, x.value_rank)
        else: 
            sort_lambda = lambda x: (x.value_rank, x.suit_rank)
        sorted_cards.sort(key = sort_lambda, reverse = True)
        print(sorted_cards)
        return sorted_cards

    def is_same_suit(self, cards = None):
        if cards is None: cards = self.cards
        cards = self.sort('suit', cards)

        if cards[0].suit == cards[-1].suit:
            print('All same suit')
            return True
        
        return False

    def occurrences(self, type = 'suit', cards = None):
        if cards is None: cards = self.cards
        
        occurrences = {}

        for card in cards:
            if type == 'suit':
                key = card.suit
            else: key = card.value

            if key in occurrences:
                occurrences[key] = occurrences[key] + 1
            else:
                occurrences[key] = 1

        return occurrences

    def card_higher_than(self, card_a, card_b):
        if card_a.suit_rank > card_b.suit_rank or card_a.value_rank > card_b.value_rank:
            return True

        return False

    def is_royal_flush(self):
        if self.is_same_suit():
            sorted = self.sort('value')
            if sorted[0].value_rank == 12 and sorted[0].value_rank == sorted[-1].value_rank + 4:
                return True
        return False

    def is_straight_flush(self):
        if self.is_same_suit():
            sorted = self.sort('value')
            if sorted[0].value_rank != 12 and sorted[0].value_rank == sorted[-1].value_rank + 4:
                return True
        return False
    
    def is_four_of_a_kind(self):
        occurrences = self.occurrences('value')

        for key,value in occurrences.items():
            if value == 4:
                return True

        return False

    def is_full_house(self):
        occurrences = self.occurrences('value')

        if len(occurrences) == 2:
            for key,value in occurrences.items():
                if value == 3 or value == 2:
                    return True

        return False

    def is_flush(self):
        if self.is_same_suit():
            sorted = self.sort('value')
            if sorted[0].value_rank != sorted[-1].value_rank + 4:
                return True
        return False

    def is_straight(self):
        if not self.is_same_suit():
            sorted = self.sort('value')
            if sorted[0].value_rank == sorted[-1].value_rank + 4:
                return True
        return False

    def is_three_of_a_kind(self):
        sorted = self.sort('value')
        if sorted[0].value_rank == sorted[2].value_rank or sorted[2].value_rank == sorted[-1].value_rank:
            return True
        return False

    def is_two_pair(self):
        occurrences = self.occurrences('value')

        if len(occurrences) == 2:
            for key,value in occurrences.items():
                if value == 3 or value == 2:
                    return True

        return False
    
    def is_one_pair(self):
        occurrences = self.occurrences('value')
        pairs = 0

        for key,value in occurrences.items():
            if value == 2:
                pairs = pairs + 1

        if pairs == 1:
            return True

        return False

    def rank(self):
        for classification in self.RANK_ORDER:
            method = getattr(self, 'is_' + classification)
            result = method()

            if result:
                print('Found ' + classification)
                return classification
                break

        return None

    # Returns True if other hand is higher
    def compare_with(self, other):
        self_rank = self.rank()
        other_rank = other.rank()
        print(self_rank, other_rank)

        # Check High Card
        if self_rank is None and other_rank is None:
            self_highest = self.sort()[0]
            other_highest = other.sort()[0]

            if self.card_higher_than(self_highest, other_highest):
                return True
            else: 
                return False

        if self_rank is None or other_rank is None:
            if self_rank is None:
                return True
            else: 
                return False

        self_rank_value = self.RANK_ORDER.index(self_rank)
        other_rank_value = self.RANK_ORDER.index(other_rank)
        print(self_rank_value, other_rank_value)

        if self_rank == other_rank:
            self_highest = self.sort()[0]
            other_highest = other.sort()[0]

            if self.card_higher_than(self_highest, other_highest):
                return False
            else: 
                return True

        if self_rank_value < other_rank_value:
            return False
        else:
            return True